module.exports = Object.freeze({
  BELT_IMAGE: 'https://i.imgur.com/sCixbGp.png',
  ELBOW_PAD_IMAGE: 'https://i.imgur.com/A2eKBPU.png',
  FLIP_FLOP_IMAGE: 'https://i.imgur.com/RYwMN7p.png',
  GLOVE_IMAGE: 'https://i.imgur.com/TcFcFx8.png',
  HAT_IMAGE: 'https://i.imgur.com/0m6MzvU.png',
  HEADBAND_IMAGE: 'https://i.imgur.com/qHjQM1I.png',
  HELMET_IMAGE: 'https://i.imgur.com/JPeIGBM.png',
  HOODIE_IMAGE: 'https://i.imgur.com/oY7zivH.png',
  JACKET_IMAGE: 'https://i.imgur.com/VVyEbhF.png',
  PANT_IMAGE: 'https://i.imgur.com/OArupGr.png',
  POOL_NOODLE_IMAGE: 'https://i.imgur.com/h3fGSnO.png',
  SHIN_GUARD_IMAGE: 'https://i.imgur.com/ojMIlkb.png',
  SHOE_IMAGE: 'https://i.imgur.com/JaIM1dj.png',
  SHORT_IMAGE: 'https://i.imgur.com/zAqJ153.png',
  SOCK_IMAGE: 'https://i.imgur.com/3ibYoGs.png',
  SUNGLASSES_IMAGE: 'https://i.imgur.com/XwHznsP.png',
  TANK_TOP_IMAGE: 'https://i.imgur.com/EMNXIJ9.png',
  VISOR_IMAGE: 'https://i.imgur.com/1Z9a17Y.png',
  WRISTBAND_IMAGE: 'https://i.imgur.com/mGNc3Vo.png'

});
