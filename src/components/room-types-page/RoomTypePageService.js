import HttpHelper from '../../utils/HttpHelper';
import constants from '../../utils/constants';

/**
 *
 * @name fetchAllRoomTypes
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setRoomTypes sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for RoomTypes if 200 response, else sets state for apiError
 */
async function fetchAllRoomTypes(setRoomTypes, setApiError) {
  await HttpHelper(constants.ROOM_TYPES_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    })
    .then(setRoomTypes)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name deleteRoomTypeById
 * @description Utilizes HttpHelper to make a DELETE request to an API
 * @param {int} productId id of product to be deleted
 * @returns a deleted product or throws an error
 */
async function deleteRoomTypeById(roomTypeId) {
  await HttpHelper(`${constants.ROOM_TYPES_ENDPOINT}/${roomTypeId}`, 'DELETE')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      if (response.status === 400) {
        throw new Error('Room Type has reviews');
      }
      throw new Error(constants.API_ERROR);
    });
}

/**
 * @name updateRoomTypeById
 * @description Utilizes HttpHelper to make a PUT request to an API
 * @param {int} RoomTypeId
 * @param {object} updatedRoomType object passed from front end form elements.
 */
async function updateRoomTypeById(updatedRoomType, roomType) {
  await HttpHelper(`${constants.ROOM_TYPES_ENDPOINT}/${roomType.id}`, 'PUT', updatedRoomType)
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      if (response.status === 400) {
        throw new Error('A server error occurred. Your updates have not been saved');
      }
      throw new Error(constants.API_ERROR);
    });
}

/**
 *
 * @name toggleRoomTypeActiveStateById
 * @description Utilizes HttpHelper to make a PUT request to an API
 * @param {int} RoomTypeId id of RoomType to be updated
 * @returns a updated RoomType or throws an error
 */
async function toggleRoomTypeActiveStateById(roomTypeId) {
  await HttpHelper(`${constants.ROOM_TYPES_ENDPOINT}/activeRoomTypes/${roomTypeId}`, 'PUT')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    });
}

export {
  fetchAllRoomTypes, deleteRoomTypeById,
  updateRoomTypeById, toggleRoomTypeActiveStateById
};
