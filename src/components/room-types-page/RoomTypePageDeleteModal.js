import { IconButton } from '@material-ui/core';
import React, { createRef, useEffect } from 'react';
import * as AiIcons from 'react-icons/ai';
import { toggleRoomTypeActiveStateById } from './RoomTypePageService';
import styles from './RoomTypePage.module.css';

/**
 * @description function for modal
 * @param closeModal toggles modal
 * @param open toggles modal
 * @param roomType will pull all of the roomTypes active and inactive
 * @param UpdateRoomTypes will update roomTypes based on edits
 * @returns component
 */

function Modal({
  closeModal, roomType, open, updateRoomType
}) {
  const toggleActive = async () => {
    await toggleRoomTypeActiveStateById(roomType.id);
    updateRoomType();
    closeModal(false);
  };
  const dialog = createRef();
  useEffect(() => {
    if (open) {
      dialog.current.showModal();
    } else {
      dialog.current.close();
    }
  }, [open, dialog]);

  return (
    <dialog ref={dialog} className={styles.modalBackground}>
      <div className={styles.modalContainer}>
        <IconButton aria-label="close modal" className={styles.modalCloseBtn} onClick={() => closeModal(false)}>
          <AiIcons.AiOutlineClose />
        </IconButton>
        <div className={styles.deleteModalTitle}>
          <h1>
            {roomType.Id}
            {' '}
            has been purchased, and therefore cannot be deleted.
          </h1>

        </div>

        <div className={styles.deleteModalBody}>
          <p>Would you like to set it to inactive instead?</p>

        </div>
        <div className={styles.footer}>

          <button
            type="submit"
            className={styles.confirmBtn}
            onClick={toggleActive}

          >
            {' '}
            <p className={styles.confirmBtnText}>
              Confirm
            </p>
            {' '}

          </button>
          <button
            type="submit"
            className={styles.cancelBtn}
            onClick={() => closeModal(false)}
          >
            {' '}
            Cancel
            {' '}
          </button>

        </div>
      </div>
    </dialog>
  );
}

export default Modal;
