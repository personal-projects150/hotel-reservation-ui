import React, { useEffect, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import styles from './RoomTypePage.module.css';
import Constants from '../../utils/constants';
import {
  fetchAllRoomTypes, updateRoomTypeById
} from './RoomTypePageService';

/**
 * @description fetches RoomTypes from API and displays in a table
 * @param {user} user logged in useer passed from the header.
 * @returns a table of all RoomTypes in the database and all their data.
 */

const RoomTypesPage = () => {
  const [roomTypes, setRoomTypes] = useState([]);
  const [apiError, setApiError] = useState(false);
  const [editedRow, setEditedRow] = useState(false);

  const updateRoomTypeList = () => fetchAllRoomTypes(setRoomTypes, setApiError);

  useEffect(() => {
    updateRoomTypeList();
  }, []);

  /**
   * @description this function takes the old product in & updates with the new product data.
   * @param {roomType} RoomType current RoomType data from the databse
   * @param {roomTypeRow} RoomTypeRow updated RoomType object.
   */
  const changeRoomType = async (roomType, roomTypeRow) => {
    await updateRoomTypeById(roomType, roomTypeRow);
    Object.assign(roomType, roomTypeRow);
  };

  return (
    <>
      {apiError && (
        <p data-testid="errMsg">
          {Constants.API_ERROR}
        </p>
      )}
      <div className={styles.roomTypesMenu}>
        <NavLink to="/createRoomTypesPage">
          <button className={styles.button} type="button">Create Room Type</button>
        </NavLink>
      </div>
      <div className={styles.roomTypesTable}>
        <table>
          <thead>
            <TableHeadings />
          </thead>
          <tbody>
            {roomTypes.sort((roomTypeA, roomTypeB) => roomTypeA.id - roomTypeB.id)
              .map((roomType) => (
                <TableData
                  key={roomType.id}
                  updateRoomType={updateRoomTypeList}
                  editedRow={editedRow}
                  setEditedRow={setEditedRow}
                  roomType={roomType}
                  changeRoomType={changeRoomType}
                />
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

/**
 * @description a row of column title headers
 * @returns component
 */
const TableHeadings = () => (
  <tr>
    <th>Edit</th>
    <th>Id</th>
    <th>Name</th>
    <th>Description</th>
    <th>Rate</th>
    <th>Active</th>
  </tr>
);

/**
 * @description a row of table data for a product
 * @param product will pull all of the RoomTypes active and inactive
 * @param UpdateProducts will update RoomTypes based on edits
 * @param purchases needs to look at if a product has purchases associated
 * @param editedRow the row containing all the updated product data
 * @param setEditedRow will set the product to the updated data.
 * @param changeProduct passes down the function of the same name from roomTypes Table function.
* @returns component
 */

const TableData = ({ roomType }) => {
  /**
   * @description displays a pencil icon. When clicked, that row becomes editable in all
   * requested fields. It then disables all other edit and delete buttons and transforms into
   * a green checkmark "Save" button.
   * @returns a pencil icon (either active or inactive depending on state).
   */
  const EditButton = () => {
    const history = useHistory();
    const onClick = () => {
      history.push(`/room-types/edit/${roomType.id}`, roomType);
    };

    return (
      <button className={styles.button} onClick={onClick} type="button">Edit</button>
    );
  };

  return (
    <tr>
      <td style={{ padding: 3 }}>
        <EditButton />
      </td>
      <td style={{ textAlign: 'center' }}>
        {roomType.id}
      </td>
      <td style={{ textAlign: 'center' }}>
        {roomType.name}
      </td>
      <td style={{ textAlign: 'center' }}>
        {roomType.description}
      </td>
      <td style={{ textAlign: 'center' }}>
        {roomType.rate}
      </td>
      <td style={{ textAlign: 'center' }}>
        {roomType.active ? 'active' : 'inactive'}
      </td>
    </tr>
  );
};

export default RoomTypesPage;
