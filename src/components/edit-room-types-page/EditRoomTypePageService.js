import HttpHelper from '../../utils/HttpHelper';
import constants from '../../utils/constants';

/**
 *
 * @name fetchRoomTypeById
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setReservations sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for reservations if 200 response, else sets state for apiError
 */
async function fetchRoomTypeById(roomTypeId, setEditRoomType) {
  await HttpHelper(`${constants.ROOM_TYPES_ENDPOINT}/${roomTypeId}`, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    })
    .then(((info) => setEditRoomType(info)));
}

export default fetchRoomTypeById;
