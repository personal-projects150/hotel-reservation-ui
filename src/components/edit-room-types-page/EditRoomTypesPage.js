import React, {
  useEffect, useState
} from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import fetchRoomTypeById from './EditRoomTypePageService';
import styles from '../create-room-types-page/CreateRoomTypePage.module.css';
import FormItem from '../form/FormItem';
import validateRoomType from './EditRoomTypeValidation';
import { updateRoomTypeById } from '../room-types-page/RoomTypePageService';

/**
 * @name EditRoomTypePage
 * @description fetches EditRoomTypess from API
 * and displays EditRoomTypess as EditRoomTypes cards
 * @return component
 */
const EditRoomTypesPage = () => {
  const history = useHistory();
  const [editRoomType, setEditRoomType] = useState(
    {
      name: '',
      description: '',
      rate: Number,
      active: 'true'
    }
  );

  const { id } = useParams();

  useEffect(() => {
    fetchRoomTypeById(Number(id), setEditRoomType);
  }, [id]);

  const onRoomTypeChange = (e) => {
    setEditRoomType({ ...editRoomType, [e.target.id]: e.target.value });
  };

  const newRoomTypeForm = {
    id: (editRoomType.id),
    name: (editRoomType.name),
    description: (editRoomType.description),
    active: (editRoomType.active),
    rate: (editRoomType.rate)
  };

  const [fieldErrors, setFieldErrors] = useState([]);

  const attemptRoomTypeCreation = () => {
    // front end validation
    const [invalidFields] = validateRoomType(newRoomTypeForm);
    // if all fields are valid
    if (Object.keys(invalidFields).length === 0) {
      updateRoomTypeById(newRoomTypeForm, { id }, setEditRoomType)
        .then(history.push('/room-types'))
        .catch(() => {
          setFieldErrors(invalidFields);
          toast.error('Room Type not updated');
        });
    } else {
      setFieldErrors(invalidFields);
      toast.error('Invalid input. Please check form for errors.');
    }
  };

  return (
    <div className={styles.wholePage}>
      <div className={styles.createRoomTypePage}>
        <h1 className={styles.h1}>Edit Room Type</h1>
        <div className={styles.productContainer}>
          <div className={styles.column1}>
            <FormItem
              type="text"
              id="name"
              label="Name"
              placeholder={editRoomType.name}
              value={newRoomTypeForm.name}
              onChange={onRoomTypeChange}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.name !== undefined && fieldErrors.name}
            </p>

            <br />
            <br />

            <FormItem
              type="number"
              id="rate"
              label="Rate"
              placeholder={editRoomType.rate}
              value={newRoomTypeForm.rate}
              onChange={onRoomTypeChange}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.rate !== undefined && fieldErrors.rate}
            </p>

            <br />
            <br />

            <FormItem
              type="textarea"
              id="description"
              label="Description"
              placeholder={editRoomType.description}
              value={newRoomTypeForm.description}
              onChange={onRoomTypeChange}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.description !== undefined && fieldErrors.description}
            </p>

            <br />
            <br />
            <div className={styles.buttonContainer}>
              <div className={styles.column3}>
                <div>
                  <strong>Active Status</strong>
                  <span className={styles.radio}>
                &nbsp;&nbsp;&nbsp;
                    <input
                      type="checkbox"
                      id="active"
                      name="status"
                      value="true"
                      checked={newRoomTypeForm.active === 'true'}
                      onChange={onRoomTypeChange}
                    />
                    active&nbsp;&nbsp;&nbsp;
                    <input
                      type="checkbox"
                      id="active"
                      name="status"
                      value="false"
                      checked={newRoomTypeForm.active === 'false'}
                      onChange={onRoomTypeChange}
                    />
                    inactive
                  </span>
                </div>
              </div>
              <p className={styles.errorMessage}>
                {fieldErrors.active !== undefined && fieldErrors.active}
              </p>
            </div>
            <br />
            <br />
          </div>
          <div className={styles.buttonContainer}>
            <button
              type="submit"
              onClick={attemptRoomTypeCreation}
              className={styles.createButton}
            >
              Update
            </button>
          </div>
        </div>
      </div>
    </div>

  );
};

export default EditRoomTypesPage;
