import React, { useState, useEffect } from 'react';
import './App.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  BrowserRouter, Route, Switch, Redirect
} from 'react-router-dom';
import HomePage from '../home-page/HomePage';
import RoomTypesPage from '../room-types-page/RoomTypesPage';
import EditRoomTypesPage from '../edit-room-types-page/EditRoomTypesPage';
import EditReservationsPage from '../edit-reservations-page/EditReservationsPage';
import ReservationsPage from '../reservations-page/ReservationsPage';
import CreateProductPage from '../create-reservations-page/CreateReservationPage';
import CreatePromoCodePage from '../create-room-types-page/CreateRoomTypePage';
import Header from '../header/Header';
import Footer from '../footer/footer';
import ProfilePage from '../profile-page/ProfilePage';
import PageNotFound from '../../utils/PageNotFound';

toast.configure();

/**
 * @name PrivateRoute
 * @description Sets Profile Page as a private route and redirects
 * website user to the home page if they are not logged in.
 */

const PrivateRoute = ({ user, setUser }) => (
  <Route
    render={() => (user ? <ProfilePage user={user} setUser={setUser} /> : <Redirect to="/home" />)}
  />
);
/**
 * @name App
 * @returns component
 */

const App = () => {
  const [user, setUser] = useState('');

  useEffect(() => {
    const loggedInUser = localStorage.getItem('user');
    if (loggedInUser !== undefined && loggedInUser !== null) {
      const foundUser = JSON.parse(loggedInUser);
      setUser(foundUser);
    }
  }, []);

  return (
    <BrowserRouter>
      <Header setUser={setUser} user={user} />
      <div id="content">
        <Switch>
          <Route exact path="/" render={() => <HomePage user={user} />} />
          <Route exact path="/room-types" render={() => <RoomTypesPage />} />
          <Route exact path="/room-types/edit/:id" render={() => <EditRoomTypesPage />} />
          <Route exact path="/reservations/edit/:id" render={() => <EditReservationsPage />} />
          <Route exact path="/reservations" render={() => <ReservationsPage user={user} />} />
          <PrivateRoute path="/profile" user={user} setUser={setUser} />
          <Route exact path="/createReservationsPage" render={() => <CreateProductPage />} />
          <Route exact path="/createRoomTypesPage" render={() => <CreatePromoCodePage />} />
          <Route render={() => <PageNotFound />} />
        </Switch>
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick={false}
          rtl={false}
          pauseOnFocusLoss={false}
          draggable={false}
          pauseOnHover={false}
        />
      </div>
      <Footer />
    </BrowserRouter>
  );
};

export default App;
