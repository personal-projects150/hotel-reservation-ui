import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NewReservationForm from './CreateReservationForm';
import styles from './CreateReservationPage.module.css';
import postNewReservation from './CreateReservationPageService';
import validateReservationCreation from './CreateReservationValidation';

/**
 * @name CreateProductPage
 * @description handles the changes when creating a new product, maps new product data
 *
 */
const CreateReservationPage = () => {
  const history = useHistory();

  const [reservationData, setReservationData] = useState({
    guestEmail: '',
    roomTypeId: Number,
    checkInDate: '',
    numberOfNights: '',
    user: 'Bob Doe'
  });

  const handleChange = (e) => {
    setReservationData({ ...reservationData, [e.target.id]: e.target.value });
  };

  const [fieldErrors, setFieldErrors] = useState({ reservation: [] });

  const attemptReservationCreation = (newReservationForm) => {
    // front end validation
    const [invalidFields] = validateReservationCreation(newReservationForm);
    // if all fields are valid
    if (Object.keys(invalidFields).length === 0) {
      postNewReservation(newReservationForm, history)
        .then(() => {
        }).catch(() => {
          setFieldErrors({ reservation: invalidFields });
          toast.error('reservation not created');
        });
    } else {
      setFieldErrors({ reservation: invalidFields });
      toast.error('Invalid input. Please check form for errors.');
    }
  };

  const handleCreate = async () => {
    const newReservationForm = {
      id: (reservationData.id),
      guestEmail: (reservationData.guestEmail),
      roomTypeId: (reservationData.roomTypeId),
      numberOfNights: (reservationData.numberOfNights),
      checkInDate: (reservationData.checkInDate),
      user: (reservationData.user)
    };
    attemptReservationCreation(newReservationForm);
  };

  return (
    <>
      <h1 className={styles.h1}>Create Reservation</h1>
      <NewReservationForm
        onChange={handleChange}
        reservationData={reservationData}
        errors={fieldErrors.reservation}
      />
      <div className={styles.buttonContainer}>
        <button
          type="submit"
          onClick={handleCreate}
          className={styles.createButton}
        >
          Create Reservation
        </button>
      </div>

    </>
  );
};
export default CreateReservationPage;
