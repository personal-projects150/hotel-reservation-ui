import React from 'react';
import FormItem from '../form/FormItem';
import FormItemDropdown from '../form/FormItemDropdown';
import styles from './CreateReservationPage.module.css';

/// summary - Builds the create product page form
const NewReservationForm = ({ onChange, reservationData, errors }) => {
  const roomTypes = [
    1, 2, 3, 4, 5, 6, 7
  ];

  return (

    <div className={styles.productContainer}>
      <div className={styles.column1}>
        <div className={errors.guestEmail === undefined ? undefined : styles.invalid}>
          <FormItem
            type="email"
            id="guestEmail"
            label="Guest Email"
            value={reservationData.guestEmail}
            onChange={onChange}
          />
          <p className={styles.errorMessage}>
            {errors.guestEmail !== undefined && errors.guestEmail}
          </p>
        </div>
        <br />
        <br />
        <div className={errors.checkInDate === undefined ? undefined : styles.invalid}>
          <FormItem
            type="text"
            id="checkInDate"
            label="Check-In Date"
            value={reservationData.checkInDate}
            onChange={onChange}
          />
          <p className={styles.errorMessage}>
            {errors.checkInDate !== undefined && errors.checkInDate}
          </p>
        </div>
        <br />
        <br />
        <div className={errors.numberOfNights === undefined ? undefined : styles.invalid}>
          <FormItem
            type="text"
            id="numberOfNights"
            label="Number of Nights"
            value={reservationData.numberOfNights}
            onChange={onChange}
          />
          <p className={styles.errorMessage}>
            {errors.numberOfNights !== undefined && errors.numberOfNights}
          </p>
        </div>
        <br />
        <br />
        <div className={errors.roomTypeId === undefined ? undefined : styles.invalid}>
          <FormItemDropdown
            type="select"
            id="roomTypeId"
            label="Room Type"
            value={reservationData.roomTypeId}
            onChange={onChange}
            options={roomTypes}
          />
          <p className={styles.errorMessage}>
            {errors.roomTypeId !== undefined && errors.roomTypeId}
          </p>
        </div>
        <br />
      </div>
    </div>
  );
};

export default NewReservationForm;
