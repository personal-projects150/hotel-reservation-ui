import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import {
  render, screen, act
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { toast } from 'react-toastify';
import CreateReservationPage from './CreateReservationPage';
import postNewReservation from './CreateReservationPageService';

jest.mock('react-toastify');
jest.mock('./CreateReservationPageService');

let container = null;
toast.configure();

describe('Create Reservation Component Tests', () => {
  function validForm() {
    userEvent.type(screen.getByText(/guest email/i), 'name@mail.com');
    userEvent.type(screen.getByText(/check-in date/i), '01-02-2023');
    userEvent.type(screen.getByText(/number of nights/i), 2);
    userEvent.type(screen.getByRole('combobox', { name: /room type/i }), 1);
  }

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('Renders the create Reservation page without crashing', () => {
    render(<CreateReservationPage />, container);
    validForm();
    expect(screen.getByText(/guest email/i)).toBeVisible();
  });

  it('Submits when all submitted info is valid', async () => {
    const toastCalls = [];
    toast.success.mockImplementation((text) => { toastCalls.push(text); });
    toast.error.mockImplementation((text) => { toastCalls.push(text); });
    // eslint-disable-next-line no-unused-vars
    postNewReservation.mockImplementation((newReservationForm) => Promise.resolve({}));

    // navigate to page
    render(<CreateReservationPage />, container);
    // fill out page with valid information
    validForm();
    // click the create Reservation button
    await act(async () => {
      userEvent.click(screen.getByRole('button', {
        name: /create reservation/i
      }));
      // get a success toast
      expect(toastCalls).toEqual('Reservation created successfully.');
    });
  });

  it('shows error msg toast when an error is thrown', () => {
    const toastCalls = [];
    toast.success.mockImplementation((text) => { toastCalls.push(text); });
    toast.error.mockImplementation((text) => { toastCalls.push(text); });
    postNewReservation.mockImplementation((newReservationForm) => {
      newReservationForm(false);
    });
    render(<CreateReservationPage />, container);

    userEvent.click(screen.getByRole('button', {
      name: /create Reservation/i
    }));

    expect(toastCalls).toEqual(['Invalid input. Please check form for errors.']);
  });
});
