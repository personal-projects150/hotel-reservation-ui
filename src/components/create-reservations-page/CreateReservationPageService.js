import { toast } from 'react-toastify';
import HttpHelper from '../../utils/HttpHelper';
import constants from '../../utils/constants';

/// summary- creates a HTTP helper function to post the new product to the API

export default async function postNewReservation(newReservationForm, history) {
  await HttpHelper(constants.RESERVATIONS_ENDPOINT, 'POST', newReservationForm)
    .then((response) => {
      if (response.ok) {
        toast.success('Reservation created successfully.');
        history.push('/reservations');
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    })
    .catch(() => {
      ('Reservation not created.');
    });
}
