/* eslint-disable react/jsx-no-bind */
import React from 'react';
import hotel from './hotel.jpeg';
// import styles from './HomePage.module.css';

/**
 * @name HomePage
 * @description fetches products from API and displays products as product cards
 * @return component
 */

const HomePage = () => (
  <div>
    <img
      src={hotel}
      alt="hotel"
      style={{
        flex: 1, flexDirection: 'row', alignSelf: 'center', maxWidth: 2000, width: '100%'
      }}
    />
  </div>
);

export default HomePage;
