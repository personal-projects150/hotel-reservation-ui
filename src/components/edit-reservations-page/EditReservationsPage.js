import React, {
  useEffect, useState
} from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import fetchReservationById from './EditReservationPageService';
import styles from '../create-reservations-page/CreateReservationPage.module.css';
import FormItem from '../form/FormItem';
import FormItemDropdown from '../form/FormItemDropdown';
import validateReservationCreation from './EditReservationValidation';
import { updateReservationById } from '../reservations-page/ReservationPageService';
import { fetchAllRoomTypes } from '../room-types-page/RoomTypePageService';

/**
 * @name EditReservationPage
 * @description fetches EditReservationss from API
 * and displays EditReservationss as EditReservations cards
 * @return component
 */
const EditReservationsPage = () => {
  const history = useHistory();
  const [editReservation, setEditReservation] = useState(
    {
      user: 'employee@catalyte.com',
      guestEmail: '',
      roomTypeId: Number,
      checkInDate: '',
      numberOfNights: Number
    }
  );
  const [roomTypes, setRoomTypes] = useState([]);
  const [roomName, setRoomName] = useState('');

  useEffect(() => {
    fetchAllRoomTypes(setRoomTypes);
  }, []);

  const getActiveRooms = () => {
    const roomObj = roomTypes.filter((room) => (room.active === true));
    if (roomObj === undefined) {
      return undefined;
    }
    const activeRooms = ([...roomObj].map((r) => (r.name)));
    return activeRooms;
  };

  const activeRoomId = (selectedRoom) => {
    const roomNm = roomTypes.find((room) => (room.name === selectedRoom));
    if (roomNm === undefined) {
      return undefined;
    }
    return roomNm.id;
  };
  const { id } = useParams();

  useEffect(() => {
    fetchReservationById(Number(id), setEditReservation);
  }, [id]);

  const onReservationChange = (e) => {
    setEditReservation({ ...editReservation, [e.target.id]: e.target.value });
  };

  const onRoomTypeChange = (e) => {
    setRoomName(e.target.value);
  };

  const newReservationForm = {
    id: (editReservation.id),
    guestEmail: (editReservation.guestEmail),
    roomTypeId: (activeRoomId(roomName)),
    numberOfNights: (editReservation.numberOfNights),
    checkInDate: (editReservation.checkInDate),
    user: ('employee@catalye.com')
  };

  const [fieldErrors, setFieldErrors] = useState([]);

  const attemptReservationCreation = () => {
    // front end validation
    const [invalidFields] = validateReservationCreation(newReservationForm);
    // if all fields are valid
    if (Object.keys(invalidFields).length === 0) {
      updateReservationById(newReservationForm, { id }, setEditReservation)
        .then(history.push('/reservations'))
        .catch(() => {
          setFieldErrors(invalidFields);
          toast.error('reservation not created');
        });
    } else {
      setFieldErrors(invalidFields);
      toast.error('Invalid input. Please check form for errors.');
    }
  };

  return (
    <div className={styles.wholePage}>
      <div className={styles.createRoomTypePage}>
        <h1 className={styles.h1}>Edit Reservation</h1>
        <div className={styles.productContainer}>
          <div className={styles.column1}>
            <FormItem
              type="email"
              id="guestEmail"
              label="Guest Email"
              placeholder={editReservation.guestEmail}
              value={newReservationForm.guestEmail}
              onChange={onReservationChange}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.guestEmail !== undefined && fieldErrors.guestEmail}
            </p>
            <br />
            <br />
            <FormItem
              type="text"
              id="checkInDate"
              label="Check-In Date"
              placeholder={editReservation.checkInDate}
              value={newReservationForm.checkInDate}
              onChange={onReservationChange}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.checkInDate !== undefined && fieldErrors.checkInDate}
            </p>
            <br />
            <br />
            <FormItem
              type="text"
              id="numberOfNights"
              label="Number of Nights"
              placeholder={editReservation.numberOfNights}
              value={newReservationForm.numberOfNights}
              onChange={onReservationChange}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.numberOfNights !== undefined && fieldErrors.numberOfNights}
            </p>
            <br />
            <br />
            <FormItemDropdown
              type="select"
              id="roomTypeId"
              label="Room Type"
              // placeholder={editReservation.roomTypeId}
              value={activeRoomId()}
              onChange={onRoomTypeChange}
              options={getActiveRooms()}
            />
            <p className={styles.errorMessage}>
              {fieldErrors.roomTypeId !== undefined && fieldErrors.roomTypeId}
            </p>
            <br />
          </div>
          <div className={styles.buttonContainer}>
            <button
              type="submit"
              onClick={attemptReservationCreation}
              className={styles.createButton}
            >
              Edit Reservation
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditReservationsPage;
