import HttpHelper from '../../utils/HttpHelper';
import constants from '../../utils/constants';

/**
 *
 * @name fetchAllReservations
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setReservations sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for reservations if 200 response, else sets state for apiError
 */
async function fetchReservationById(reservationId, setEditReservation) {
  await HttpHelper(`${constants.RESERVATIONS_ENDPOINT}/${reservationId}`, 'GET')
    .then((response) => {
      if (response.ok) {
        // history.push('/reservations');
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    })
    .then(((info) => setEditReservation(info)));
}

export default fetchReservationById;
