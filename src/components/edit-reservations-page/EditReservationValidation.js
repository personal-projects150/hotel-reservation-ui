/**
 * Validates that a field is not empty
 * @param {string} field
 * @returns a boolean
*/
const isEmpty = (field) => {
  if (field === undefined || field === null || field.trim().length === 0) {
    return true;
  }
  return false;
};

/**
   * Validates that a quantity only contains numbers
   * @param {string} quantity
   * @returns an empty string if valid, otherwise an error message
  */
const validateNumOfNights = (numberOfNights) => {
  if (isEmpty(numberOfNights.toString())) {
    return 'This field is required';
  }
  if ((/^\d+$/).test(numberOfNights) && numberOfNights > 0) {
    return '';
  }
  return 'Must be a whole number greater than 0.';
};

/**
   * Validates than an email address only has alphanumeric characters in the username
   * and only alphabetical characters in the domain name.
   * @param {string} guestEmail
   * @returns
   */
const validateGuestEmail = (guestEmail) => {
  if (isEmpty(guestEmail)) {
    return 'Required';
  }
  if ((/^\w+@([a-z]+\.)+[a-z]+$/i).test(guestEmail)) {
    return '';
  }
  return 'Must be formatted as user@email.com';
};

/**
   * Validates a date
   * @param {string} checkInDate
   * @returns an empty string if valid, otherwise an error message
  */
const validateCheckInDate = (checkInDate) => {
  if (isEmpty(checkInDate.toString())) {
    return 'This field is required';
  }
  if
  ((/^(0[1-9]|1[0-2])([-]{1})\d{2}([-]{1})(\d{4})$/).test(checkInDate)) {
    return '';
  }
  return 'Must be in ##/##/####';
};
  /**
   * Validates that an image source
   * @param {string} imageSrc
   * @returns an empty string if valid, otherwise an error message
  */
const validateRoomType = (roomTypeId) => {
  const roomToString = roomTypeId.toString();
  if (isEmpty(roomToString)) {
    return 'This field is required';
  }

  if
  ((/[^]/)
    .test(roomToString)) {
    return '';
  }
  return '';
};

/**
   * Validates all required fields
   * @param {Object} input
   * @returns an object with field errors given as {field: 'message'}
   */
const validateReservationCreation = ({
  guestEmail, roomTypeId, numberOfNights, checkInDate
}) => {
  const invalidFields = {};

  const emailValidation = validateGuestEmail(guestEmail);
  if (emailValidation) {
    invalidFields.guestEmail = emailValidation;
  }
  const roomTypeValidation = validateRoomType(roomTypeId);
  if (roomTypeValidation) {
    invalidFields.roomTypeId = roomTypeValidation;
  }
  const numOfNightsValidation = validateNumOfNights(numberOfNights);
  if (numOfNightsValidation) {
    invalidFields.numberOfNights = numOfNightsValidation;
  }
  const dateValidation = validateCheckInDate(checkInDate);
  if (dateValidation) {
    invalidFields.checkInDate = dateValidation;
  }
  return [{ ...invalidFields }];
};

export default validateReservationCreation;
