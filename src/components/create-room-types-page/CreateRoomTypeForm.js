import React from 'react';
import FormItem from '../form/FormItem';
import styles from './CreateRoomTypePage.module.css';

/// summary - Builds the create product page form
const NewRoomTypeForm = ({ onChange, roomTypeData, errors }) => (

  <div className={styles.productContainer}>
    <div className={styles.column1} />
    <br />
    <br />
    <div className={errors.name === undefined ? undefined : styles.invalid}>
      <FormItem
        type="text"
        id="name"
        label="Name"
        value={roomTypeData.name}
        onChange={onChange}
      />
      <p className={styles.errorMessage}>
        {errors.name !== undefined && errors.name}
      </p>
    </div>
    <br />
    <br />
    <div className={errors.description === undefined ? undefined : styles.invalid}>
      <FormItem
        type="textarea"
        id="description"
        label="Description"
        value={roomTypeData.description}
        onChange={onChange}
      />
      <p className={styles.errorMessage}>
        {errors.description !== undefined && errors.description}
      </p>
    </div>
    <br />
    <br />
    <div className={errors.rate === undefined ? undefined : styles.invalid}>
      <FormItem
        type="number"
        id="rate"
        label="Rate"
        value={roomTypeData.rate}
        onChange={onChange}
      />
      <p className={styles.errorMessage}>
        {errors.rate !== undefined && errors.rate}
      </p>
    </div>
    <p className={styles.errorMessage}>
      {errors.active !== undefined && errors.active}
    </p>
    <br />
  </div>

);

export default NewRoomTypeForm;
