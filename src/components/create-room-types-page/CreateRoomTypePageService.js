import { toast } from 'react-toastify';
import HttpHelper from '../../utils/HttpHelper';
import constants from '../../utils/constants';

/// summary- creates a HTTP helper function to post the new product to the API

export default async function postNewRoomType(newRoomTypeForm, history) {
  await HttpHelper(constants.ROOM_TYPES_ENDPOINT, 'POST', newRoomTypeForm)
    .then((response) => {
      if (response.ok) {
        toast.success('RoomType created successfully.');
        history.push('/room-types');
        return response.json();
      }
      throw new Error(constants.API_ERROR);
    })
    .catch(() => {
      ('Room Type not created.');
    });
}
