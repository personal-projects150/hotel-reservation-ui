import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NewRoomTypeForm from './CreateRoomTypeForm';
import styles from './CreateRoomTypePage.module.css';
import postNewRoomType from './CreateRoomTypePageService';
import validateRoomType from './CreateRoomTypeValidation';

/**
 * @name CreateProductPage
 * @description handles the changes when creating a new product, maps new product data
 *
 */
const CreateRoomTypePage = () => {
  const history = useHistory();

  const [roomTypeData, setRoomTypeData] = useState({
    active: 'true',
    name: '',
    description: '',
    rate: Number

  });

  const handleChange = (e) => {
    setRoomTypeData({ ...roomTypeData, [e.target.id]: e.target.value });
  };

  const [fieldErrors, setFieldErrors] = useState({ roomType: [] });

  const attemptRoomTypeCreation = (newRoomTypeForm) => {
    // front end validation
    const [invalidFields] = validateRoomType(newRoomTypeForm);
    // if all fields are valid
    if (Object.keys(invalidFields).length === 0) {
      postNewRoomType(newRoomTypeForm, history)
        .then(() => {
        }).catch(() => {
          setFieldErrors({ roomType: invalidFields });
          toast.error('roomType not created');
        });
    } else {
      setFieldErrors({ roomType: invalidFields });
      toast.error('Invalid input. Please check form for errors.');
    }
  };

  const handleCreate = async () => {
    const newRoomTypeForm = {
      active: roomTypeData.active === 'true',
      name: roomTypeData.name,
      rate: roomTypeData.rate,
      description: roomTypeData.description
    };
    attemptRoomTypeCreation(newRoomTypeForm);
  };

  return (
    <div className={styles.wholePage}>
      <div className={styles.createRoomTypePage}>
        <h1 className={styles.h1}>Create Room Type</h1>
        <NewRoomTypeForm
          onChange={handleChange}
          roomTypeData={roomTypeData}
          errors={fieldErrors.roomType}
        />
        <div className={styles.column3}>
          <div>
            <strong>Active Status</strong>
            <span className={styles.radio}>
                &nbsp;&nbsp;&nbsp;
              <input
                type="checkbox"
                id="active"
                name="status"
                value="true"
                checked={roomTypeData.active === 'true'}
                onChange={handleChange}
              />
              active&nbsp;&nbsp;&nbsp;
              <input
                type="checkbox"
                id="active"
                name="status"
                value="false"
                checked={roomTypeData.active === 'false'}
                onChange={handleChange}
              />
              inactive
            </span>
          </div>
        </div>
        <div className={styles.buttonContainer}>
          <button
            type="submit"
            onClick={handleCreate}
            className={`${styles.createButton} ${styles.column4}`}
          >
            Create Room Type
          </button>
        </div>
      </div>
    </div>
  );
};
export default CreateRoomTypePage;
