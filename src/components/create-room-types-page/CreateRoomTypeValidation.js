/**
 * Validates whether field is empty
 * @param {string} field
 * @returns boolean
 */
const isEmpty = (field) => {
  if (field === undefined || field === null || field.trim().length === 0) {
    return true;
  }
  return false;
};

/**
 * Validates that the name is uppercase and numerical
 * @param {string} name
 * @returns empty string if valid, otherwise an error message
 */
const validateName = (name) => {
  if (isEmpty(name)) {
    return 'This field is required';
  }
  if (name.length > 3) {
    return '';
  }
  return 'Must be at least 3 characters.';
};

/**
 * Validates that the rate is present
 * @param {string} rate
 * @returns empty string if valid, otherwise an error message
 */
const validateRate = (rate) => {
  if (isEmpty(rate.toString())) {
    return 'This field is required';
  }
  if (rate > 0) {
    return '';
  }
  return 'Must be number greater than zero.';
};

/**
 * validates an attempted promo code
 * @param {string} name
 * @param {string} rate
 * @returns an object
 */
const validateRoomType = ({
  name, rate
}) => {
  const invalidFields = {};

  const nameValidation = validateName(name);
  if (nameValidation) {
    invalidFields.name = nameValidation;
  }
  const rateValidation = validateRate(rate);
  if (rateValidation) {
    invalidFields.rate = rateValidation;
  }

  return [{ ...invalidFields }];
};

export default validateRoomType;
