import HttpHelper from '../../utils/HttpHelper';
import Constants from '../../utils/constants';

/**
 *
 * @name fetchAllReservations
 * @description Utilizes HttpHelper to make a get request to an API
 * @param {*} setReservations sets state for products
 * @param {*} setApiError sets error if response other than 200 is returned
 * @returns sets state for reservations if 200 response, else sets state for apiError
 */
async function fetchAllReservations(setReservations, setApiError) {
  await HttpHelper(Constants.RESERVATIONS_ENDPOINT, 'GET')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .then(setReservations)
    .catch(() => {
      setApiError(true);
    });
}

/**
 *
 * @name deleteReservationById
 * @description Utilizes HttpHelper to make a DELETE request to an API
 * @param {int} productId id of product to be deleted
 * @returns a deleted product or throws an error
 */
async function deleteReservationById(reservationId, setApiError) {
  await HttpHelper(`${Constants.RESERVATIONS_ENDPOINT}/${reservationId}`, 'DELETE')
    .then((response) => {
      if (response.NoContent) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    })
    .catch(() => {
      // Account for errors caused by backend changes since page render
      setApiError(true);
    });
}

/**
 * @name updateReservationById
 * @description Utilizes HttpHelper to make a PUT request to an API
 * @param {int} reservationId
 * @param {object} updatedreservation object passed from front end form elements.
 */
async function updateReservationById(updatedReservation, reservation) {
  await HttpHelper(`${Constants.RESERVATIONS_ENDPOINT}/${reservation.id}`, 'PUT', updatedReservation)
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      if (response.status === 400) {
        throw new Error('A server error occurred. Your updates have not been saved');
      }
      throw new Error(Constants.API_ERROR);
    });
}

/**
 *
 * @name toggleReservationActiveStateById
 * @description Utilizes HttpHelper to make a PUT request to an API
 * @param {int} ReservationId id of Reservation to be updated
 * @returns a updated Reservation or throws an error
 */
async function toggleReservationActiveStateById(reservationId) {
  await HttpHelper(`${Constants.RESERVATIONS_ENDPOINT}/activeReservations/${reservationId}`, 'PUT')
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(Constants.API_ERROR);
    });
}

export {
  fetchAllReservations, deleteReservationById,
  updateReservationById, toggleReservationActiveStateById
};
