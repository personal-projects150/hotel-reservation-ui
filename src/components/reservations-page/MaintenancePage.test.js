import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import { render, screen, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ReservationPage from './ReservationsPage';
import { fetchAllReservations, deleteReservationById } from './ReservationPageService';
import { fetchUserPurchase } from '../profile-page/ProfilePageService';
// import { createArrayLiteral } from 'typescript';

jest.mock('./ReservationPageService');
jest.mock('../profile-page/ProfilePageService');
jest.mock('react-toastify');
let container = null;

toast.configure();
describe('Reservation Page Component Tests', () => {
  beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('shows error msg text when an error is thrown', () => {
    fetchAllReservations.mockImplementation((setReservations, setApiError) => {
      setApiError(true);
    });
    render(
      <BrowserRouter>
        <ReservationPage />
      </BrowserRouter>, container
    );
    expect(screen.getByTestId('errMsg')).toHaveTextContent('Oops, something went wrong');
  });

  it('shows 21 columns in each row and one row per Reservation', () => {
    fetchAllReservations.mockImplementation((setReservations, setApiError) => {
      setApiError(false);
      setReservations([{ id: 0, price: 80.00 }, { id: 1, price: 80.00 }]);
    });
    render(
      <BrowserRouter>
        <ReservationPage />
      </BrowserRouter>, container
    );
    expect(screen.getAllByRole('columnheader').length).toBe(21);
    expect(screen.getAllByRole('row').length).toEqual(3);
  });

  it('sorts Reservations based on id', () => {
    fetchAllReservations.mockImplementation((setReservations, setApiError) => {
      setApiError(false);
      setReservations([{ id: 5, price: 80.00 },
        { id: 999999999, price: 80.00 },
        { id: 0, price: 80.00 }]);
    });
    render(
      <BrowserRouter>
        <ReservationPage />
      </BrowserRouter>, container
    );
    const rows = screen.getAllByRole('row');
    expect(rows[0].childNodes[1].textContent).toBe('ID');
    expect(rows[1].childNodes[1].textContent).toBe('0');
    expect(rows[2].childNodes[1].textContent).toBe('5');
    expect(rows[3].childNodes[1].textContent).toBe('999999999');
  });

  it('displays delete button only when Reservation has no reviews', () => {
    fetchAllReservations.mockImplementation((setReservations, setApiError) => {
      setApiError(false);
      setReservations([{ id: 5, price: 80.00 },
        { id: 1, price: 80.00, reviewCount: 0 },
        { id: 2, price: 80.00, reviewCount: 10 }]);
    });
    render(
      <BrowserRouter>
        <ReservationPage />
      </BrowserRouter>, container
    );
    const rows = screen.getAllByRole('row');
    expect(rows[1].childNodes[0].childNodes.length).toBe(1);
    expect(rows[2].childNodes[0].childNodes.length).toBe(0);
  });

  it('toasts a success message when a Reservation is deleted', async () => {
    fetchAllReservations.mockImplementation((setReservations, setApiError) => {
      setApiError(false);
      setReservations([{
        name: 'testReservation', id: 1, price: 10.00, reviewCount: 0
      }]);
    });
    deleteReservationById.mockImplementation(async (ReservationId) => ReservationId);
    const toastCalls = [];
    toast.success.mockImplementation((text) => { toastCalls.push(text); });
    render(
      <BrowserRouter>
        <ReservationPage />
      </BrowserRouter>, container
    );
    await act(async () => {
      userEvent.click(screen.getByTestId('delete 1'));
    });
    expect(toastCalls).toEqual(['testReservation successfully deleted.']);
  });
  it('toasts an error message when Reservation is not deleted', async () => {
    fetchAllReservations.mockImplementation((setReservations, setApiError) => {
      setApiError(false);
      setReservations([{ id: 1, price: 10.00, reviewCount: 0 }]);
    });
    deleteReservationById.mockImplementation(async (ReservationId) => {
      throw Error(ReservationId);
    });
    fetchUserPurchase.mockImplementation();
    const toastCalls = [];
    toast.error.mockImplementation((text) => { toastCalls.push(text); });
    render(
      <BrowserRouter>
        <ReservationPage />
      </BrowserRouter>, container
    );
    await act(async () => {
      userEvent.click(screen.getByTestId('delete 1'));
    });
    expect(toastCalls).toEqual(['Server Error. Reservation not deleted. Please try again.']);
  });

  // it('modal opens when attempt to delete a Reservation that has been purchased', async () => {
  //   fetchAllReservations.mockImplementation((setReservations, setApiError) => {
  //     setApiError(false);
  //     setReservations([{
  //       name: 'testReservation', id: 1, price: 10.00, reviewCount: 0, active: true
  //     }]);
  //   });
  //   fetchUserPurchase.mockImplementation((setPurchase) => {
  //     setPurchase([{
  //       // object that looks like a purchase
  //       lineItems: [
  //         {
  //           ReservationId: 1,
  //           quantity: 1,
  //           ReservationName: 'Next Gen Soccer Wristband'
  //         }
  //       ]
  //     }]);
  //   });
  //   deleteReservationById.mockImplementation(async (ReservationId) => ReservationId);
  //   const toastCalls = [];
  //   toast.success.mockImplementation((text) => { toastCalls.push(text); });
  //   render(
  //     <BrowserRouter>
  //       <ReservationPage />
  //     </BrowserRouter>, container
  //   );
  //   await act(async () => {
  //     userEvent.click(screen.getByTestId('delete 1'));
  //   });
  //   expect(screen.getByText('Next Gen Soccer Wristband')).toBeEnabled();
  // });
});
