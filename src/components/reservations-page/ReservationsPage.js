import React, { useEffect, useState } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import IconButton from '@material-ui/core/IconButton';
import { toast } from 'react-toastify';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import FormItem from '../form/FormItem';
import styles from './MaintenancePage.module.css';
import Constants from '../../utils/constants';
import {
  fetchAllReservations, deleteReservationById, updateReservationById
} from './ReservationPageService';
import Modal from './MaintenancePageDeleteModal';
import { fetchAllRoomTypes } from '../room-types-page/RoomTypePageService';

/**
 * @description fetches reservations from API and displays in a table
 * @param {user} user logged in useer passed from the header.
 * @returns a table of all reservations in the database and all their data.
 */

const ReservationsPage = () => {
  const [reservations, setReservations] = useState([]);
  const [roomTypes, setRoomTypes] = useState([]);
  const [apiError, setApiError] = useState(false);
  const [editedRow, setEditedRow] = useState(false);
  const updateReservationList = () => fetchAllReservations(setReservations, setApiError);
  const allRoomTypes = () => fetchAllRoomTypes(setRoomTypes, setApiError);

  useEffect(() => {
    updateReservationList();
    allRoomTypes();
  }, []);

  /**
   * @description this function takes the old product in & updates with the new product data.
   * @param {reservation} reservation current reservation data from the databse
   * @param {reservation} reservationRow updated reservation object.
   */
  const changeReservation = async (reservation, reservationRow) => {
    await updateReservationById(reservation, reservationRow);
    Object.assign(reservation, reservationRow);
  };

  return (
    <div className={styles.wholePage}>
      {apiError && (
        <p data-testid="errMsg">
          {Constants.API_ERROR}
        </p>
      )}
      <div className={styles.maintenanceMenu}>
        <NavLink to="/createReservationsPage">
          <button className={styles.button} type="button">Create Reservation</button>
        </NavLink>
      </div>
      <div className={styles.maintenanceTable}>
        <table>
          <thead>
            <TableHeadings />
          </thead>
          <tbody>
            {reservations.sort((reservationA, reservationB) => reservationA.id - reservationB.id)
              .map((reserve) => (
                <TableData
                  key={reserve.id}
                  updateReservation={updateReservationList}
                  editedRow={editedRow}
                  setEditedRow={setEditedRow}
                  reservation={reserve}
                  changeReservation={changeReservation}
                  allRoomTypes={roomTypes}
                />
              ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

/**
 * @description a row of column title headers
 * @returns component
 */
const TableHeadings = () => (
  <tr>
    <th>Edit/Delete</th>
    <th>Id</th>
    <th>Guest Email</th>
    <th>Room Type</th>
    <th>Check In Date</th>
    <th>Number Of Nights</th>
    <th>Total Cost</th>
  </tr>
);

/**
 * @description a row of table data for a product
 * @param product will pull all of the reservations active and inactive
 * @param UpdateProducts will update reservations based on edits
 * @param purchases needs to look at if a product has purchases associated
 * @param editedRow the row containing all the updated product data
 * @param setEditedRow will set the product to the updated data.
 * @param changeProduct passes down the function of the same name from Maintenance Table function.
* @returns component
 */

const TableData = ({
  reservation, updateReservation, editedRow, setEditedRow, changeReservation, allRoomTypes
}) => {
  const [editReservation, setEditReservation] = useState(false);
  const [reservationRow, setReservationRow] = useState({});

  const onReservationChange = (event) => {
    setReservationRow({ ...reservationRow, [event.target.id]: event.target.value });
  };

  function getRoomType(id, roomTypes) {
    const roomObj = roomTypes.find((room) => room.id === id);
    if (roomObj === undefined) {
      return undefined;
    } return roomObj;
  }

  function totalCost() {
    const roomTypeObj = getRoomType(reservation.roomTypeId, allRoomTypes);
    if (roomTypeObj === undefined) {
      return undefined;
    }
    const product = Math.round((roomTypeObj.rate * reservation.numberOfNights) * 100) / 100;
    return product;
  }

  /**
   * @description checks for the status of the active checkbox in edit mode and assigns active
   * status according to that.
   */
  const setReservationActiveStatus = () => {
    if (document.getElementById('active').checked === true) {
      reservationRow.active = true;
    } else {
      reservationRow.active = false;
    }
  };

  const [apiError, setApiError] = useState(false);

  const DeleteButton = () => {
    const [clickable] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    const deleteReservation = () => {
      if (clickable) {
        deleteReservationById(reservation.id, setApiError)
          .then(() => {
            updateReservation();
            toast.success(` Reservation ${reservation.id} successfully deleted.`);
          });
      }
    };

    const onClick = () => {
      if (clickable) {
        deleteReservation();
      }
    };

    return (
      !editedRow ? (
        <>
          <IconButton onClick={onClick} color="inherit" size="small" className={styles.rightButton} data-testid={`delete ${reservation.id}`}>
            <button className={styles.button} type="button">Delete</button>
          </IconButton>
          {openModal && (
          <Modal
            closeModal={setOpenModal}
            reservation={reservation}
            confirmFn={deleteReservation}
            open={openModal}
            updateReservation={updateReservation}
          />
          )}
        </>
      ) : <button className={styles.disabledDeleteButton} type="button">Delete</button>
    );
  };

  /**
   * @description displays a pencil icon. When clicked, that row becomes editable in all
   * requested fields. It then disables all other edit and delete buttons and transforms into
   * a green checkmark "Save" button.
   * @returns a pencil icon (either active or inactive depending on state).
   */
  const EditButton = () => {
    const history = useHistory();
    const onClick = () => {
      history.push(`/reservations/edit/${reservation.id}`, reservation);
      setEditedRow(true);
      setEditReservation(!editReservation);
    };

    return (
      <button className={styles.button} onClick={onClick} type="button">Edit</button>
    );
  };

  /**
   * @description displays a green checkmark button. When clicked, it submits all changes made
   * to the reservation fields to the database and then reverts the row back to view mode & returns
   * functionality to all edit/delete buttons on page
   * @returns a green checkmark button.
   */
  const SaveButton = () => {
    const [clickable] = useState(true);

    const onClick = () => {
      if (clickable) {
        setReservationActiveStatus();
        changeReservation(reservation, reservationRow)
          .then(() => {
            toast.success(`${reservation.name} successfully updated.`);
            setEditReservation(false);
            setEditedRow(false);
          })
          .catch(() => {
            toast.error('A server error occurred. Your updates have not been saved.');
          });
      }
    };
    return (
      <FontAwesomeIcon
        onClick={onClick}
        icon={faCheckCircle}
        color="#7CEA9C"
        size="1x"
        className={styles.leftButton}
      />
    );
  };

  /**
   * @description displays a red x button. When clicked, it cancels changes made to edit fields
   * and then reverts the row back to view mode & returns functionality to all edit/delete buttons
   * on page.
   * @returns a red x button
   */

  return (
    <tr>
      {apiError && (
        <p data-testid="errMsg">
          {Constants.API_ERROR}
        </p>
      )}
      <td style={{ padding: 3 }}>
        {!editReservation ? (
          <EditButton />
        ) : (
          <SaveButton
            reservation={reservation}
          />
        )}
        {!editReservation && (
          <DeleteButton
            reservation={reservation}
            updatereservation={updateReservation}
          />
        )}

      </td>
      <td>
        {(editReservation && (
          <FormItem
            placeholder={reservation.id}
            defaultValue={reservation.id}
            type="text"
            id="id"
            onChange={onReservationChange}
            value={reservationRow.id}
          />
        )) || reservation.id}
      </td>
      <td>
        {(editReservation && (
          <FormItem
            placeholder={reservation.guestEmail}
            defaultValue={reservation.guestEmail}
            type="text"
            id="guestEmail"
            onChange={onReservationChange}
            value={reservationRow.guestEmail}
          />
        )) || reservation.guestEmail}
      </td>
      <td>
        {(editReservation && (
          <FormItem
            placeholder={reservation.roomTypeId}
            defaultValue={reservation.roomTypeId}
            type="text"
            id="roomTypeId"
            onChange={onReservationChange}
            value={reservationRow.roomTypeId}
          />
        )) || reservation.roomTypeId}
      </td>
      <td>
        {(editReservation && (
          <FormItem
            placeholder={reservation.checkInDate}
            defaultValue={reservation.checkInDate}
            type="text"
            id="checkInDate"
            onChange={onReservationChange}
            value={reservationRow.checkInDate}
          />
        )) || reservation.checkInDate}
      </td>
      <td>
        {(editReservation && (
          <FormItem
            placeholder={reservation.numberOfNights}
            defaultValue={reservation.numberOfNights}
            type="text"
            id="numberOfNights"
            onChange={onReservationChange}
            value={reservationRow.numberOfNights}
          />
        )) || reservation.numberOfNights}
      </td>
      <td>{totalCost()}</td>
    </tr>

  );
};

export default ReservationsPage;
