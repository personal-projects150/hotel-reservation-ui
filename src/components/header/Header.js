import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Header.module.css';

const Header = () => (

  <section className={styles.header}>
    <div className={styles.reservations}>
      <NavLink activeClassName={styles.active} to="/reservations" style={{ color: 'whitesmoke' }}>
        Reservations
      </NavLink>
    </div>
    <div className={styles.title}>
      <NavLink to="/">
        Hotel Bookings
      </NavLink>
    </div>
    <div className={styles.roomType}>
      <NavLink activeClassName={styles.active} to="/room-types" style={{ color: 'whitesmoke' }}>
        Room Types
      </NavLink>
    </div>
  </section>
);
export default Header;
