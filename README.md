# Hotel Bookings, Inc.

Hotel Bookings, a hotel management software company, has approached Catalyte to build a frontend application for their reservations and room-types API. It needs to allow managers at a hotel to add, edit, delete reservations for guests, as well as let managers add and edit the types of rooms available for booking. 

## Install Prerequisites

### Node Version Manager (NVM)

NVM is a utility to help you quickly install and switch between Node versions. With NVM, there is no need to manually install and uninstall versions.

Follow the Installation Steps for [NVM on GitHub](https://github.com/coreybutler/nvm-windows).

## Getting Started

1. Clone this project locally.
1. CD into the root folder
1. Run `npm install` in the root folder to install dependencies.

This command installs a package, and any packages that it depends on.

1. Run `npm start`.

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Dependencies
* Hotel API must be running. Confer with team resources if you are unsure.

## Testing
* You can run tests with coverage via `npm run test:coverage`
